package de.favo.exercisecalculator;

import java.io.Serializable;

/**
 *     Exercise Calculator
 *     Copyright (C) 2016  Felix von Oertzen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Calculation implements Serializable{
    static final int OPERATION_MULTIPLY = 34;
    static final int OPERATION_DIVIDE = 35;
    static final int OPERATION_ADD = 36;
    static final int OPERATION_SUBTRACT = 37;
    static final int OPERATION_POTENTIZE = 38;

    private Integer a;
    private int calculation_method;
    private Integer b;
    private Integer putativeResult;


    Calculation(Integer a, Integer b, int operation) {
        this.a = a;
        this.b = b;
        calculation_method = operation;
    }




    public String getA() {
        return a.toString();
    }

    public String getB() {
        return b.toString();
    }

    public String getCalculationMethod() {
        if (calculation_method == OPERATION_MULTIPLY)
            return "×";
        if (calculation_method == OPERATION_POTENTIZE)
            return "^";
        if (calculation_method == OPERATION_SUBTRACT)
            return "–";
        if (calculation_method == OPERATION_DIVIDE)
            return "÷";
        if (calculation_method == OPERATION_ADD)
            return "+";

        return "?";
    }

    public boolean isCorrect() {
        return !((getPutativeResult() == null) || (getCorrectResult() == null)) && getPutativeResult().equals(getCorrectResult());
    }

    public void setPutativeResult(Integer putativeResult) {
        this.putativeResult = putativeResult;
    }
    public Integer getPutativeResult() {
        return putativeResult;
    }
    public Integer getCorrectResult() {
        if (calculation_method == OPERATION_ADD)
                return a + b;
        if (calculation_method == OPERATION_MULTIPLY)
            return a * b;
        if (calculation_method == OPERATION_SUBTRACT)
            return a - b;
        if (calculation_method == OPERATION_DIVIDE)
            return a / b;
        if (calculation_method == OPERATION_POTENTIZE)
            return (int) Math.pow(a, b);


        return null;
    }

    public String getCalculation() {
        if (calculation_method != OPERATION_POTENTIZE)
            return getA() + getCalculationMethod() + getB();
        else
            return "<html>" + getA() + "<sup><small>" + getB() + "</small></sup></html>";
    }

    public String getComparableString(){
        return a + getCalculationMethod() + b;
    }

    public static int max(int a, int b){
        if (a == b)
            return a;
        if (a > b)
            return a;
        if (a < b)
            return b;
        return 0;
    }
    public static int min(int a, int b){
        if (a == b)
            return a;
        if (a > b)
            return b;
        return a;
    }
}
