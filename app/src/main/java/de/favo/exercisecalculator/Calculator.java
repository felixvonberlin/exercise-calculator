package de.favo.exercisecalculator;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

/**
 *     Exercise Calculator
 *     Copyright (C) 2016  Felix von Oertzen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Calculator extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Spinner min1;
    private Spinner min2;
    private Spinner max1;
    private Spinner max2;
    private Spinner number;
    private Spinner type;
    private LinearLayout repeating_info;
    @SuppressWarnings("FieldCanBeLocal")
    private ImageButton repeat_info;
    private Vector<String> additional;
    private ArrayAdapter<String> adapter;
    private SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sp = getSharedPreferences("cvjdnkjdf", MODE_PRIVATE);
        additional = new Vector<>();
        FloatingActionButton start = findViewById(R.id.calculator_start);
        min1 = findViewById(R.id.calculator_min_first);
        min2 = findViewById(R.id.calculator_min_second);
        max1 = findViewById(R.id.calculator_max_first);
        max2 = findViewById(R.id.calculator_max_second);
        number = findViewById(R.id.calculator_number_of_tasks);
        type = findViewById(R.id.calculator_operator);
        repeat_info     =   findViewById(R.id.repeat_info_help);
        repeating_info  =   findViewById(R.id.repeat_info);

        ArrayList<String>range = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.number_range)));
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, range);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        min1.setAdapter(adapter);
        min1.setOnItemSelectedListener(this);
        min2.setAdapter(adapter);
        min2.setOnItemSelectedListener(this);
        max1.setAdapter(adapter);
        max1.setOnItemSelectedListener(this);
        max2.setAdapter(adapter);
        max2.setOnItemSelectedListener(this);

        repeat_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(Calculator.this)
                        .setTitle(R.string.text_some_tasks_duplicate)
                        .setMessage(R.string.text_some_tasks_duplicate_explanation)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();
            }
        });
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Calculator.this,Exercise.class);
                int amount,operation,start1, start2, end1, end2;
                int[] operations = {
                        Calculation.OPERATION_ADD,
                        Calculation.OPERATION_SUBTRACT,
                        Calculation.OPERATION_MULTIPLY,
                        Calculation.OPERATION_DIVIDE,
                        Calculation.OPERATION_POTENTIZE};
                amount      =    Integer.parseInt(number.getSelectedItem().toString());
                start1      =    Integer.parseInt(min1.getSelectedItem().toString());
                start2      =    Integer.parseInt(min2.getSelectedItem().toString());
                end1        =    Integer.parseInt(max1.getSelectedItem().toString());
                end2        =    Integer.parseInt(max2.getSelectedItem().toString());
                operation   =    operations[type.getSelectedItemPosition()];
                {
                    // Check for sense-full parameters
                    boolean ok;
                    ok =        start1 <= end1;
                    ok = ok  && start2 <= end2;
                    if (!ok){
                        new AlertDialog.Builder(Calculator.this)
                                .setTitle(R.string.useless)
                                .setMessage(R.string.useless_input_text)
                                .setPositiveButton(R.string.change_it, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                }).show();
                        return;
                    }
                }

                i.putExtra(Exercise.EXTRA_AMOUNT, amount);
                i.putExtra(Exercise.EXTRA_OPERATION, operation);
                i.putExtra(Exercise.EXTRA_FIRST_RANGE_BEGIN, start1);
                i.putExtra(Exercise.EXTRA_SECOND_RANGE_BEGIN, start2);
                i.putExtra(Exercise.EXTRA_FIRST_RANGE_END, end1);
                i.putExtra(Exercise.EXTRA_SECOND_RANGE_END, end2);
                startActivity(i);
            }
        });
        restoreState();
        if (sp.getBoolean("primeravec", true))
            startActivity(new Intent(getApplicationContext(),StartActivity.class));
    }
    private void saveState(){
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("additional_C",additional.size());
        for (int i = 0; i < additional.size(); i++)
            editor.putString("additional_"+i,additional.elementAt(i));
        editor.putInt("min1",min1.getSelectedItemPosition());
        editor.putInt("max1",max1.getSelectedItemPosition());
        editor.putInt("min2",min2.getSelectedItemPosition());
        editor.putInt("max2",max2.getSelectedItemPosition());
        editor.putInt("operator",type.getSelectedItemPosition());
        editor.putInt("amount", number.getSelectedItemPosition());
        editor.apply();
    }
    private void restoreState(){
        for (int i = 0; i < sp.getInt("additional_C",0); i++) {
            additional.add(sp.getString("additional_" + i, ""));
            adapter.insert(additional.elementAt(i),adapter.getCount()-1);
        }
        min1.setSelection(sp.getInt("min1",0));
        min2.setSelection(sp.getInt("min2",0));
        max1.setSelection(sp.getInt("max1",0));
        max2.setSelection(sp.getInt("max2",0));
        number.setSelection(sp.getInt("amount",0));
        type.setSelection(sp.getInt("operator",0));
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calculator, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_about) {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://sites.google.com/view/favoapps/"));
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onItemSelected(final AdapterView<?> parent, final View view, final int position, long id) {
        if (adapter.getCount()-1 != position) {
            //if (1==1)
              //  return;
            try {
                int amount, start1, start2, end1, end2;
                amount = Integer.parseInt(number.getSelectedItem().toString());
                start1 = Integer.parseInt(min1.getSelectedItem().toString());
                start2 = Integer.parseInt(min2.getSelectedItem().toString());
                end1 = Integer.parseInt(max1.getSelectedItem().toString());
                end2 = Integer.parseInt(max2.getSelectedItem().toString());
                if ((end1-start1 < 0)||(end2-start2 < 0)) {
                    repeating_info.setVisibility(View.INVISIBLE);
                    return;
                }
                repeating_info
                        .setVisibility((amount > CalculationManager.getPossibleTasks(start1, start2, end1, end2))
                                ? View.VISIBLE : View.INVISIBLE);
            }catch (Exception e){
                repeating_info.setVisibility(View.INVISIBLE);
            }
            return;
        }
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        new AlertDialog.Builder(this)
                .setMessage(R.string.enter_own_value)
                .setView(input)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String editable = input.getText().toString();
                        additional.add(editable);
                        adapter.setNotifyOnChange(true);
                        adapter.insert(editable,position);
                        adapter.notifyDataSetChanged();
                        ((Spinner)parent).setSelection(position,true);

                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        parent.setSelection(0);
                    }
                }).show();

    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
