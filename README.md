[![License](https://img.shields.io/badge/License-GNU%20General%20Public%20License%20v3.0-green.svg?logo=gnu)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![Android](https://img.shields.io/badge/designed%20for-Android-green.svg?logo=android&color=a4c639)](https://www.android.com)
[![Java](https://img.shields.io/badge/using-Java-blue.svg?logo=java&color=007396)](http://www.oracle.com/technetwork/java/index.html)

# Exercise Calculator
It's a simple app for improving your calculation skills.

## Where do I get the App?
You can compile it your self, or get it on Google Play:

[https://play.google.com/store/apps/details?id=de.favo.exercisecalculator](https://play.google.com/store/apps/details?id=de.favo.exercisecalculator)

## Why I developed such an app?
The reason was not boredom; My little brother had to learn the "small 1x1",

so i thought about helping him with a little app.
