package de.favo.exercisecalculator;

import java.io.Serializable;
import java.util.Vector;

/**
 *     Exercise Calculator
 *     Copyright (C) 2016  Felix von Oertzen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class CalculationManager implements Serializable {
    private int count = -1;
    private int size;
    private Vector<Calculation> calculations;

    public CalculationManager(int type, int count, int min1, int min2, int max1, int max2,State s) {
        RandomManager randMan1 = new RandomManager(min1, max1);
        RandomManager randMan2 = new RandomManager(min2, max2);
        calculations = new Vector<>(count);
        size = count;
        if (type != 0) {
            Calculation c;
            Integer a;
            Integer b;
            boolean stop = false;
            int possibleTasks = getPossibleTasks(min1, min2, max1, max2);
            int multiple = (int) (((double) count / (double) possibleTasks));
            boolean duplicate = possibleTasks <= count;
            while (!stop) {
                a = (randMan1.getInt());
                b = (randMan2.getInt());
                c = new Calculation(a, b, type);
                if (!duplicate){
                    if ((isCalculationInIt(calculations, c) < 1)) {
                        calculations.add(c);
                        s.onProgressChanged(calculations.size(),size);
                    }
                }
                else if (!(isCalculationInIt(calculations, c) > multiple)) {
                        calculations.add(c);
                        s.onProgressChanged(calculations.size(),size);
                    }
                if (calculations.size() == size)
                    stop = true;
            }
        }
    }

    private int isCalculationInIt(Vector<Calculation> vector, Calculation i) {
        int count = 0;
        for (Calculation c : vector)
            if (c.getComparableString().equals(i.getComparableString()))
                count += 1;
        return count;
    }

    public Calculation next() {
        count = count + 1;
        if (count < calculations.size())
            return calculations.elementAt(count);
        return null;
    }
    public Calculation current(){
        return getCalculations().elementAt(getPosition());
    }

    public void reset() {
        count = 0;
    }

    public boolean hasNext() {
        return count < calculations.size() - 1;
    }

    public Calculation elementAt(int elementAt) {
        return calculations.elementAt(elementAt);
    }

    public Vector<Calculation> getCalculations() {
        return calculations;
    }

    public int getSize() {
        return size;
    }

    public int getPosition() {
        return count;
    }

    public int getSuccessRate() {
        int corr = 0;
        for (Calculation c : calculations)
            if (c.isCorrect())
                corr++;
        return (int) (((double) corr) / ((double) getCalculations().size()) * 100);
    }

    public static int getPossibleTasks(int s1, int s2, int e1, int e2) {
        if (e1 - s1 == 0)
            e1 = e1 + 1;
        if (e2 - s2 == 0)
            e2 = e2 + 1;
        return (e1-s1)*(e2-s2);
    }
    interface State{
        void onProgressChanged(int state, int target);
    }
}
