package de.favo.exercisecalculator;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;

/**
 *     Exercise Calculator
 *     Copyright (C) 2016  Felix von Oertzen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class ResultActivity extends AppCompatActivity {
    protected static final String EXTRA_MGR = "calcMgr";
    private boolean leave = false;
    private WebView view;
    private String data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        view = findViewById(R.id.result_view);
        Button save = findViewById(R.id.save_result);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(
                                ResultActivity.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED) {
                        saveScreenshot();
                    }else{
                        if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                            new AlertDialog.Builder(ResultActivity.this)
                                    .setTitle(R.string.app_name)
                                    .setMessage(R.string.why_need_external_storage)
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        @TargetApi(Build.VERSION_CODES.M)
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},508);
                                        }
                                    })
                                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {


                                        }
                                    }).show();
                        else
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},508);
                    }
                }else{
                    saveScreenshot();
                }
            }
        });

        if (savedInstanceState == null)
            data = generateHTML();
        else
          data = savedInstanceState.getString("html");
        view.loadData(data,"text/html; charset=utf-8", "utf-8");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 508: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    saveScreenshot();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(this, R.string.permission_denied, Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    /**
     * Sends a broadcast to have the media scanner scan a file
     *
     * @param path
     *            the file to scan
     */
    private void scanMedia(String path) {
        File file = new File(path);
        Uri uri = Uri.fromFile(file);
        Intent scanFileIntent = new Intent(
                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri);
        sendBroadcast(scanFileIntent);
    }
    private void saveScreenshot() {
        Bitmap scr = getScreenshot();
        FileOutputStream fos;
        try {
            final File root = new File(Environment.getExternalStorageDirectory()+"/"+getString(R.string.app_name));
            if (!root.mkdir())
                Log.w("Storage","Cannot create Dir");
            final String fileName;
            if (Build.VERSION.SDK_INT <= 17)
                fileName = new SimpleDateFormat("yyyyMMddkkmm'.jpg'", Locale.getDefault()).format(new Date());
            else
                fileName = new SimpleDateFormat("yyyyMMddHHmm'.jpg'", Locale.getDefault()).format(new Date());

            fos = new FileOutputStream(new File(root,fileName));
            scr.compress(Bitmap.CompressFormat.JPEG, 100, fos );
            fos.close();
            Snackbar.make(findViewById(R.id.result_layout),
                    R.string.screenshot_saved,Snackbar.LENGTH_LONG)
                    .setAction(R.string.open, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        Uri uri;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                            uri = FileProvider.getUriForFile(ResultActivity.this,
                                BuildConfig.APPLICATION_ID + ".provider",new File(root,fileName));
                        else
                            uri = Uri.fromFile(new File(root,fileName));
                        intent.setDataAndType(uri, "image/*");
                        intent.setFlags(FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(intent);

                }
            }).show();
            scanMedia(new File(root,fileName).getPath());

        }
        catch( Exception e ) {
            System.out.println("-----error--"+e);
        }
    }
    private String generateHTML() {
        CalculationManager cm = (CalculationManager)getIntent().getSerializableExtra(EXTRA_MGR);
        StringBuilder ret = new StringBuilder();
        int proc = cm.getSuccessRate();
        ret.append("<html>\n");
        ret.append("    <body>\n");
        ret.append("        <h2 style=\"text-align: center;\">").append(getString(R.string.result_of_exercise)).append("</h2>\n");
        ret.append("        <p style=\"text-align: center;\">").append((SimpleDateFormat.getDateInstance()).format(new Date(System.currentTimeMillis()))).append("</p>");
        ret.append("        <p style=\"text-align: center;\">").append(getString(R.string.text_percent, proc + "%25")).append("</p>");
        ret.append("        <table style=\"text-align: center;\" cellpadding=\"10\" width=\"100%25\" border=\"0\">");
        ret.append("            <tr bgcolor=\"#A2A2A2\">");
        ret.append("                <th>").append(getString(R.string.text_task)).append("</th>");
        ret.append("                <th>").append(getString(R.string.text_your_result)).append("</th>");
        ret.append("                <th>").append(getString(R.string.text_solution)).append("</th>");
        ret.append("            </tr>");

        for (Calculation c : cm.getCalculations()) {
            if (c.isCorrect())
                ret.append("            <tr bgcolor=\"#63E556\">");
            else
                ret.append("            <tr bgcolor=\"#E5565D\">");
            ret.append("                <td>").append(c.getCalculation()).append("</td>");
            ret.append("                <td>").append(c.getPutativeResult()).append("</td>");
            ret.append("                <td>").append(c.getCorrectResult()).append("</td>");
            ret.append("            </tr>");
        }
        ret.append("        </table>");
        ret.append("    </body>\n");
        ret.append("</html>\n");
        return ret.toString();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (data != null)
            outState.putString("html",data);
    }
    private Bitmap getScreenshot(){
        /*Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
Canvas canvas = new Canvas(bitmap);
myWebView.draw(canvas);*/
        @SuppressWarnings("deprecation")
        Picture picture = view.capturePicture();
        Bitmap b = Bitmap.createBitmap(
                picture.getWidth(), picture.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        picture.draw(c);
        return b;
    }
    @Override
    public void onBackPressed() {
        if (leave)
            startActivity(new Intent(ResultActivity.this,Calculator.class));
        else{
            Toast.makeText(this,R.string.press_back_to_return,Toast.LENGTH_LONG).show();
            leave = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    leave = false;
                }
            },2000);
        }
    }
}
